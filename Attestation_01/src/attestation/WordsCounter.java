package attestation;

import java.io.*;
import java.util.*;

//C:\Users\FreshTime\IdeaProjects\Attestation_01\src\baskervilles.txt
//C:\Users\FreshTime\IdeaProjects\Attestation_01\src\theGalaxy.txt
public class WordsCounter {

    public static void main(String[] args) throws WordsException {
        WordsCounter wordsCounter = new WordsCounter();
        wordsCounter.run();
    }

    private void run() {
        System.out.println("Введите путь нужного Вам файла: ");
        Scanner scanner = new Scanner(System.in);
        String fileName = scanner.nextLine();
        String allText = readAllAsText(fileName);
        process(allText);


    }

    private String readAllAsText(String fileName) throws WordsException {
        File text = new File(fileName);
        StringBuilder bld = new StringBuilder();
        try (Scanner scnr = new Scanner(text)) {
            while (scnr.hasNextLine()) {
                String line = scnr.nextLine();
                bld.append(line);
                bld.append(" ");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "";
        }
        return bld.toString();
    }

    private List<String> readAllLines(String fileName) {
        File text = new File(fileName);
        List<String> res = new ArrayList<>();
        try (Scanner scnr = new Scanner(text)) {
            while (scnr.hasNextLine()) {
                String line = scnr.nextLine();
                res.add(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
        return res;

    }

    private void process(String param) {
        String cleanString = preProcess(param);
        String[] words = cleanString.split(" ");
        Set<String> uniqWords = new HashSet<>();
        Map<String, Integer> frequency = new HashMap<>();

        for (String currWord : words) {
            String keyWord = currWord.toLowerCase();
            uniqWords.add(keyWord);
            frequency.putIfAbsent(keyWord, 0);
            Integer currFreq = frequency.get(keyWord);
            frequency.put(keyWord, currFreq + 1);
        }


        File file = new File("ResultWord.txt");
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, false))) {
            try {
                file.createNewFile();
                System.out.println("Файл с подсчетом создан");

            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            for (Map.Entry<String, Integer> result : frequency.entrySet()) {
                try {
                    writer.write(result.getKey() + " = " + result.getValue() + "\n");
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String preProcess(String param) {
        StringBuilder builder = new StringBuilder();
        char[] k = param.toCharArray();
        for (int i = 0; i < k.length; i++) {
            char t = k[i];
            if (t == '.' || t == ';' || t == '?' || t == ',' || t == '"' || t == '!'
                    || t == '—' || t == '”' || t == '“' || t == '’' || t == '‘') {
                continue;
            }
            if (t == ' ' && i < k.length - 1 && k[i + 1] == ' ') {
                continue;
            }
            builder.append(t);
        }
        return builder.toString();
    }
}

