import org.junit.jupiter.api.Test;

import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class PalidromTest {

    @Test
    public void Test1() {

        Palidrom t = new Palidrom();
        String s = "1235321";
        assert t.isPalidrom(s) == true;
    }

    @Test
    public void Test2 () {

        Palidrom t2 = new Palidrom();
        String s = "1177711";
        assert t2.isPalidrom(s) == true;
    }

    @Test
    public void Test3 () {

        Palidrom t3 = new Palidrom();
        String s = "1123457";
        assert t3.isPalidrom(s) == false;
    }

    @Test
    public void Test4 () {

        Palidrom t4 = new Palidrom();
        String s = "114565411";
        assert t4.isPalidrom(s) == true;

    }

    @Test
    public void Test5 () {

        Palidrom t5 = new Palidrom();
        String s = "114567";
        assert t5.isPalidrom(s) == false;

    }
}

