package OOP;

/*
Сделать абстрактный класс Figure, у данного класса есть два поля - x и y координаты.
Классы Ellipse и Rectangle должны быть потомками класса Figure.
Класс Square - потомок класса Rectangle, Circle - потомок класса Ellipse.
В классе Figure предусмотреть абстрактный метод getPerimeter().
Так же, нужно определить интерфейс Moveable c единственным методом .move(int x, int y), который позволит перемещать фигуру на заданные координаты.
Данный интерфейс должны реализовать только классы Circle и Square.
В Main создать массив всех фигур и "перемещаемых" фигур. У всех вывести в консоль периметр,
а у "перемещаемых" фигур ИЗМЕНИТЬ СЛУЧАЙНЫМ ОБРАЗОМ КООРДИНАТЫ.
 */

public class Circle extends Ellipse implements Moveable{

    private double isConst;

    public Circle(double x, double y, double radius, double isConst) {
        super(x, y, radius);
        this.isConst = isConst;
    }


    @Override
    public double getPerimeter() {
        double rez= getRadius() * isConst;
        System.out.println("Периметр круга: " + rez);
        return rez;
    }

    @Override
    public void move(int x, int y) {
        x = (int) (getX() + x);
        y = (int) (getY() +y);
        System.out.println ("Координаты круга по оси X измененны на: " + x  + "\n" +
                "Координаты круга по оси Y измененны на: " +  y );
    }
    public void randomMove (int x, int y) {
        x = x;
        y = y;
        System.out.println ("Координаты круга по оси X измененны случайным образом на: " + x  + "\n" +
                "Координаты круга по оси Y измененны случайным образом: " +  y );
    }
}


