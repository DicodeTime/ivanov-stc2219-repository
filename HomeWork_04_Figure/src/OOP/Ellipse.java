package OOP;

/*
Сделать абстрактный класс Figure, у данного класса есть два поля - x и y координаты.
Классы Ellipse и Rectangle должны быть потомками класса Figure.
Класс Square - потомок класса Rectangle, Circle - потомок класса Ellipse.
В классе Figure предусмотреть абстрактный метод getPerimeter().
Так же, нужно определить интерфейс Moveable c единственным методом .move(int x, int y), который позволит перемещать фигуру на заданные координаты.
Данный интерфейс должны реализовать только классы Circle и Square.
В Main создать массив всех фигур и "перемещаемых" фигур. У всех вывести в консоль периметр,
а у "перемещаемых" фигур ИЗМЕНИТЬ СЛУЧАЙНЫМ ОБРАЗОМ КООРДИНАТЫ.
 */

public class Ellipse extends Figure{
    private double bigAxis;
    private double smallAxis;
    private double radius;

    public Ellipse(double x, double y, double bigAxis, double smallAxis) {
        super(x, y);
        this.bigAxis = bigAxis;
        this.smallAxis = smallAxis;
    }

    public Ellipse(double x, double y, double radius) {
        super(x, y);
        this.radius = radius;
    }

    public double getBigAxis() {
        return bigAxis;
    }

    public void setBigAxis(double bigAxis) {
        this.bigAxis = bigAxis;
    }

    public double getSmallAxis() {
        return smallAxis;
    }

    public void setSmallAxis(double smallAxis) {
        this.smallAxis = smallAxis;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getPerimeter() {
      double rez = (4 * ((Math.PI * bigAxis* smallAxis)+((bigAxis-smallAxis)*(bigAxis-smallAxis))))/ (bigAxis+smallAxis);
      System.out.println("Периметр эллипса: " + rez);
      return rez;
    }
}
