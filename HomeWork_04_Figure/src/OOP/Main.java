package OOP;
/*
Сделать абстрактный класс Figure, у данного класса есть два поля - x и y координаты.
Классы Ellipse и Rectangle должны быть потомками класса Figure.
Класс Square - потомок класса Rectangle, Circle - потомок класса Ellipse.
В классе Figure предусмотреть абстрактный метод getPerimeter().
Так же, нужно определить интерфейс Moveable c единственным методом .move(int x, int y), который позволит перемещать фигуру на заданные координаты.
Данный интерфейс должны реализовать только классы Circle и Square.
В Main создать массив всех фигур и "перемещаемых" фигур. У всех вывести в консоль периметр,
а у "перемещаемых" фигур ИЗМЕНИТЬ СЛУЧАЙНЫМ ОБРАЗОМ КООРДИНАТЫ.
 */
public class Main {
    public static void main(String[] args) {


        Rectangle rectangle = new Rectangle(2,8, 10, 5);
        Ellipse ellipse = new Ellipse(10,8,8, 4);


        Square square = new Square(4,3, 8,4);
        System.out.println("Координаты квадрата по оси X: " + square.getX() + "\n" +
                "Координаты квадрата по оси Y: " +  square.getY());

        Circle circle = new Circle(4, 5, 8, 2*Math.PI);
        System.out.println("Координаты круга" + " по оси X: " + circle.getX() + "\n" +
                "Координаты круга" + " по оси Y: " +  circle.getY());

        System.out.println();
        circle.randomMove((int) (Math.random()*20),(int) (Math.random()*20));
        System.out.println();
        circle.move(2,2);
        System.out.println();

        square.randomMove((int) (Math.random()*10),(int) (Math.random()*10));
        System.out.println();
        square.move(3,3);
        System.out.println();

        Figure[] figures = {rectangle,ellipse, circle ,square};
        for (int i=0; i<figures.length; i++){
            figures[i].getPerimeter();
        }





    }
}
