package OOP;

/*
Сделать абстрактный класс Figure, у данного класса есть два поля - x и y координаты.
Классы Ellipse и Rectangle должны быть потомками класса Figure.
Класс Square - потомок класса Rectangle, Circle - потомок класса Ellipse.
В классе Figure предусмотреть абстрактный метод getPerimeter().
Так же, нужно определить интерфейс Moveable c единственным методом .move(int x, int y), который позволит перемещать фигуру на заданные координаты.
Данный интерфейс должны реализовать только классы Circle и Square.
В Main создать массив всех фигур и "перемещаемых" фигур. У всех вывести в консоль периметр,
а у "перемещаемых" фигур ИЗМЕНИТЬ СЛУЧАЙНЫМ ОБРАЗОМ КООРДИНАТЫ.
 */

public class Rectangle extends Figure {

    private double length;
    private double width;
    private  double  side;

    public Rectangle(int x, int y, double length, double width) {
        super(x, y);
        this.length = length;
        this.width = width;
    }

    public Rectangle(double x, double y, double side) {
        super(x, y);
        this.side = side;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    @Override
    public double getPerimeter() {
        double rez = (length + width) * 2;
        System.out.println("Периметр прямоугольника: " + rez);
        return rez;
    }
}
