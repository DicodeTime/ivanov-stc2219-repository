package OOP;

import org.junit.jupiter.api.Test;

class MainTest {

    @Test
    public void Test_Ellipse_1(){
        Ellipse ellipse1 =new Ellipse(1,1, 2, 4);
        assert  ellipse1.getPerimeter()== 19.42182748581223;
    }
    @Test
    public void Test_Ellipse_2(){
        Ellipse ellipse2 =new Ellipse(3,3, 4, 6);
        assert  ellipse2.getPerimeter()== 31.759289474462015;
    }
    @Test
    public void Test_Ellipse_3(){
        Ellipse ellipse3 =new Ellipse(2,2, 10, 8);
        assert  ellipse3.getPerimeter()== 56.73942495270743;
    }

    @Test
    public void Test_Circle_1(){
        Circle circle = new Circle(1,1,6, 2*Math.PI);
        assert circle.getPerimeter()== 37.69911184307752;

    }
    @Test
    public void Test_Circle_2(){
        Circle circle2 = new Circle(1,1,8,2*Math.PI);
        assert circle2.getPerimeter()== 50.26548245743669;

    }
    @Test
    public void Test_Circle_3() {
        Circle circle3 = new Circle(11, 9,10,2 * Math.PI);
        assert circle3.getPerimeter() == 62.83185307179586;
    }


    @Test
    public void Test_Rectangle_1(){
        Rectangle rectangle1 = new Rectangle(8,8, 4, 8);
        assert rectangle1.getPerimeter() == 24;
    }
    @Test
    public void Test_Rectangle_2(){
        Rectangle rectangle2 = new Rectangle(3,1, 12, 20);
        assert rectangle2.getPerimeter() ==  64;
    }
    @Test
    public void Test_Rectangle_3(){
        Rectangle rectangle3 = new Rectangle(4,4, 10, 15);
        assert rectangle3.getPerimeter() == 50;
    }
    @Test
    public void Test_Square_1(){
        Square square1 = new Square(6,5,20,4);
        assert square1.getPerimeter()== 80;
    }
    @Test
    public void Test_Square_2(){
        Square square2 = new Square(7,7,15,4);
        assert square2.getPerimeter()== 60;
    }
    @Test
    public void Test_Square_3(){
        Square square3 = new Square(1,1,8,4);
        assert square3.getPerimeter()== 32;
    }

}