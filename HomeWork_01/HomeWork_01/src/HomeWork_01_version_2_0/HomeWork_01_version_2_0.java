package HomeWork_01_version_2_0;

import java.util.Scanner;

public class HomeWork_01_version_2_0 {
        public static void main (String[] args){
            Scanner sc = new Scanner(System.in);
            System.out.print("Введите десятичное число: ");
            int i= sc.nextInt();
            String s=toBin(i,8);
            System.out.println(s );
            System.out.print("Введите двоичное число: ");
            s=  sc.next();
            System.out.println( toDec(s));

        }

        public static String toBin (int i, int n_bit) // функция для перевода числа в  двоичный вид
        {
            String sRet="";       // пустая строка для формирования результата
            for (int x=n_bit; x!=0; x--) // цикл по количеству бит
            {
                if ((i & 1<<n_bit-1)!=0)
                    sRet+="1"; // если старший бит не "0" то прибавляем единичку к строке
                else sRet+="0"; // а иначе "0" к строке
                i<<=1; // сдвиг на один разряд влево
            }
            return sRet; // возврат результата
        }

        public static int toDec(String s) // функция для перевода  двоичной строки в десятичный вид
        {
            int iRet=0; // начальное значение результирующей переменной
            for (int x=0; x<s.length(); x++)  // цикл прохождение длины строки
                if (s.charAt(x) != '0')
                    iRet += (1 << (s.length()-x-1)); // если символ не равен '0' то прибавляем вес(..128,64,32,16,8,4,2,1)
            return iRet;
        }
    }


