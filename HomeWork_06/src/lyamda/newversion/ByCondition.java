package lyamda.newversion;

@FunctionalInterface
public interface ByCondition {
    boolean isOk(int number);

}
