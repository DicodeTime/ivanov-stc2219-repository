package lyamda.newversion;

/*
Предусмотреть функциональный интерфейс:
interface ByCondition {
    boolean isOk(int number);
}
    Написать класс Sequence, в котором должен присутствовать метод filter:
public static int[] filter(int[] array, ByCondition condition) {
        ...
        }
        Данный метод возвращает массив, который содержит элементы, удовлетворяющиие логическому выражению в condition.
        В main в качестве condition подставить:
        проверку на четность элемента
        проверку, является ли сумма цифр элемента четным числом.
        Доп. задача: проверка на четность всех цифр числа. (число состоит из цифр)
        Доп. задача: проверка на палиндромность числа (число читается одинаково и слева, и справа -> 11 - палиндром, 12 - нет, 3333 - палиндром, 3211 - нет, и т.д.).
*/

import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {

        Random random = new Random();
        int[] array = new int[10];

        for (int i = 0; i < array.length; i++) {

            array[i] = random.nextInt(100) + 1;

        }

        ByCondition conditionOne = (number) -> number % 2 == 0;

        ByCondition conditionTwo = (number) -> {
            int sum = 0;
            while (number != 0) {
                sum += (number % 10);
                number /= 10;
            }
            return (sum % 2 == 0);
        };

        ByCondition conditionThree = (number) -> {
            boolean evenNum = true;
            while (number != 0) {
                evenNum &= ((number % 10) % 2 == 0);
                number /= 10;
            }
            return evenNum;
        };

        ByCondition conditionFour = (number) -> {
            boolean result = true;
            String str = "" + number;
            for (int i = 0; i < str.length(); i++)
                result &= (str.charAt(i) == str.charAt(str.length() - i - 1));
            return result;
        };

        int[] evenNumber = new int[10];
        int[] sumNumElem = new int[10];
        int[] evenNumAll = new int[10];
        int[] isPalidromNum = new int[10];
        int[][] result = {evenNumber, sumNumElem, evenNumAll, isPalidromNum};
        ByCondition[] conditions = {conditionOne, conditionTwo, conditionThree, conditionFour};

        System.out.println(Arrays.toString(array));
        System.out.println();

        for (int i = 0; i < result.length; i++) {
            result[i] = Sequence.filter(array, conditions[i]);
            System.out.println(Arrays.toString(result[i]));
        }

    }
}