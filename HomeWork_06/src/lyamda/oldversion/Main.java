package lyamda.oldversion;

/*
Предусмотреть функциональный интерфейс:
interface ByCondition {
    boolean isOk(int number);
}
    Написать класс Sequence, в котором должен присутствовать метод filter:
public static int[] filter(int[] array, ByCondition condition) {
        ...
        }
        Данный метод возвращает массив, который содержит элементы, удовлетворяющиие логическому выражению в condition.
        В main в качестве condition подставить:
        проверку на четность элемента
        проверку, является ли сумма цифр элемента четным числом.
        Доп. задача: проверка на четность всех цифр числа. (число состоит из цифр)
        Доп. задача: проверка на палиндромность числа (число читается одинаково и слева, и справа -> 11 - палиндром, 12 - нет, 3333 - палиндром, 3211 - нет, и т.д.).
*/

import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {

        Random random = new Random();
        int[] array = new int[10];

        for (int i = 0; i < array.length; i++) {

            array[i] = random.nextInt(1000) + 1;

        }

        int[] evenNumber = Sequence.filter(array, number -> number % 2 == 0);

        int[] sumNumElem = Sequence.filter(array, number -> {
            int sum = 0;
            while (number != 0) {
                sum += (number % 10);
                number /= 10;
            }
            return (sum % 2 == 0);
        });

        int[] evenNumAll = Sequence.filter(array, number -> {
            boolean evenNum = true;
            while (number != 0) {
                evenNum &= ((number % 10) % 2 == 0);
                number /= 10;
            }
            return evenNum;
        });

        int[] isPalidromNum = Sequence.filter(array, number -> {
            boolean result = true;
            String str = "" + number;
            for (int i = 0; i < str.length(); i++)
                result &= (str.charAt(i) == str.charAt(str.length() - i - 1));
            return result;
        });

        System.out.println(Arrays.toString(array));
        System.out.println();
        System.out.println(Arrays.toString(evenNumber));
        System.out.println(Arrays.toString(sumNumElem));
        System.out.println(Arrays.toString(evenNumAll));
        System.out.println(Arrays.toString(isPalidromNum));

    }
}