package lyamda.oldversion;

public class Sequence {

    public static int[] filter(int[] array, ByCondition condition) {

        int newSizeArray = 0;
        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])) {
                newSizeArray++;
            }
        }
        int[] result = new int[newSizeArray];
        int nextIndex = 0;
        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])) {
                result[nextIndex] = array[i];
                nextIndex++;
            }

        }
        return result;
    }

}




