package functions;

public class HomeWork_02_functions {
    public static void main(String[] args) {
        int[] array = new int[]{0, 1, 2, 4, 5, 0, 578, 0, 980, 3, 0}; // массив с целочислеными данными

        System.out.print(retArray(array,5)); // вызов функции и вывод в консоль
    }
    public static int retArray ( int[] array, int number) // функция возвращения индекса числа в массиве
    {
        for (int x = 0; x < array.length; x++) // цикл проверки числа в массиве
        {
            if (array[x] == number) // сравнение с элементом числа в массиве
            {
                return x; // возврат индекса числа
            }
        }
        return -1; // -1 возвращается если в массиве нет заданного числа
    }
}
