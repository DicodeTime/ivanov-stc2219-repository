package functions;


import java.util.Scanner;

//** эксперемент с рандомными числами и вводом с клавиатуры

public class HomeWork_02_functions_version_2_0 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in); // ввод числа с клавиатуры
        int[] array = new int[10]; // длинна массива 10
        for (int x = 0; x < array.length; x++) {  // цикл для проверки длинны массива
            array[x] = (int) (Math.random()*10); //  инициализирую число случайными числами

            System.out.print(array[x]+" ");

        }System.out.println();

        System.out.print(retArray(array, sc.nextInt())); // вывод результата в консоль
    }
    public static int retArray ( int[] array, int number) // функция возвращения индекса числа в массиве
    {
        for (int x = 0; x < array.length; x++) // цикл проверки числа в массиве
        {
            if (array[x] == number) // сравнение с элементом числа в массиве
            {
                return x; // возврат индекса числа
            }
        }
        return -1; // -1 возвращается если в массиве нет заданного числа
    }
}