package procedures;
import java.util.Arrays;

public class HomeWork_02_procedures {

    public static void main(String[] args) {

        int [] array = new int[] {34,0,0,0,14,15,0,18,0,0,1,20}; //  массив с заданными значениями
        System.out.println(Arrays.toString(array)); // вывод в консоль исходного массива для визуального сравнение с результатом
        swapArray(array,7, 1); // функция меняет местами элементы массива с заданными индексами
        swapArray(array,10, 2);
        swapArray(array,11, 3);
        System.out.println(Arrays.toString(array)); // вывод результата в консоль

    }


    public static void swapArray (int array[], int index1, int index2){  //процедура обмена элементов массива

        int temp = array[index1]; // временная переменная для сохранения первого элемента массива
        array[index1] = array [index2] ;
        array[index2] = temp;
    }
}
