package procedures;
import java.util.Arrays;

public class HomeWork_02_procedures_version_2_0 {

    public static void main(String[] args) {

        int [] array = new int[] {34,0,0,0,14,15,0,18,0,0,1,20}; //  массив с заданными значениями
        int [][] config = new int[][]{{7,1},{10,2},{11,3}}; //  индексы элементов массива для обмена значениями
        System.out.println(Arrays.toString(array));
        for (int y=0; y<config.length; y++){  // цикл по элементам конфигурационного массива
            swapArray(array, config[y][0], config[y][1]); // обмен значениями
        }
        System.out.println(Arrays.toString(array));
    }

    public static void swapArray (int array[], int index1, int index2){ //процедура обмена элементов массива

        array[index1] ^= array [index2] ; //обмен элементов через исключающее "ИЛИ"
        array[index2] ^= array[index1];
        array[index1] ^= array[index2];

    }
}

