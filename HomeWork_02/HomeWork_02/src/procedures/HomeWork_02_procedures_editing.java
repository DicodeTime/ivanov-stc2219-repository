package procedures;
import java.util.Arrays;

public class HomeWork_02_procedures_editing {

    public static void main(String[] args) {

        int[] array = new int[]{34, 0, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        System.out.println(Arrays.toString(array));
        swapArray(array);
        System.out.println(Arrays.toString(array));
    }

    public static void swapArray ( int array[]){
            for (int i = 0; i < array.length; i++) {
                for (int k = i + 1; k < array.length; k++) {
                    if (array[i] < array[k]) {
                        array[i] ^= array[k];
                        array[k] ^= array[i];
                        array[i] ^= array[k];
                    }
                }
            }
        }
}





