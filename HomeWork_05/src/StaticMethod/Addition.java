package StaticMethod;
/*
Реализовать класс со статическими методами:
Сложение всех чисел, которые передаются в метод. Метод возвращает результат сложения.
 */
public class Addition {

    public static int getAddition(int... values) {

        int rezult = 0;

        for (int i = 0; i < values.length; i++) {

            rezult += values[i];
        }

        return rezult;
    }

}
