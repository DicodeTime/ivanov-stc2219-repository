package StaticMethod;

/*
Реализовать класс со статическими методами:
Вычитание всех чисел, которые передаются в метод. Метод возвращает результат вычитания.
Доп. задание: найти наибольшее число из чисел, которые передали в метод. И производить вычитание из него.
*/
public class Subtraction {

    public static int getSubtraction(int... values) {
        int sumNum = 0;
        int maxNum = values[0];
        int maxIndex = 0;
        for (int i = 0; i < values.length; i++) {
            if (values[i] > maxNum) {
                maxNum = values[i];
                maxIndex = i;
            }
            sumNum += values[i];
        }

        return maxNum - (sumNum - maxNum);
    }


}
