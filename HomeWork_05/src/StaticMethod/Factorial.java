package StaticMethod;
/*
Реализовать класс со статическими методами:
Метод, высчитывающий факториал переданного числа. Должна быть проверка, что переданное число положительное.
 */
public class Factorial {

    public static int getFactorial(int num) {
        if (num > 0) {
            if (num <= 1) {

                return 1;
            } else {

                return num * getFactorial(num - 1);

            }
        }
        return -1;
    }
}
