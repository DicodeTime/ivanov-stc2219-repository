package StaticMethod;
/*
Реализовать класс со статическими методами:
Умножение всех чисел, которые передаются в метод. Метод возвращает результат умножения.
*/
public class Multiplication {

    public static int getMultiplication(int... values) {

        int rezult = 1;

        for (int i = 0; i < values.length; i++) {

            rezult *= values[i];
        }
        return rezult;
    }

}

