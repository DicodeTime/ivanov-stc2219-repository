package StaticMethod;

/*
Реализовать класс со статическими методами:
Деление всех чисел, которые передаются в метод. При каждом следующем делении должна идти проверка:
что делимое число должно быть больше делителя. Если условие не выполнено, то метод возвращает текущий результат деления.
что делитель - положительное число. Если условие не выполнено, то метод возвращает текущий результат деления.
 */
public class Division {

    public static int getDivision(int... values) {

        int div = values[0];
        int rez = 0;

        for (int i = 1; i < values.length; i++) {
            rez = div / values[i];
            if (i + 1 < values.length) {
                boolean flag = ((rez > values[i + 1]) && (values[i + 1] > 0));
                if (flag) {
                    div = rez;
                } else
                    return rez;
            }
        }
        return rez;
    }
}