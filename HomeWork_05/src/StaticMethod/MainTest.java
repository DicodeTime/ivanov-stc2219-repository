package StaticMethod;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    @Test
    public void Test_1_Add() {

        int[] arr = {2, 3, 4, 6, 7, 8, 9, 0};
        assert Addition.getAddition(arr) == 39;
        assert Addition.getAddition(2, 2, 2, 2, 2) == 10;


    }
    @Test
    public void Test_2_Sub() {

        int[] arr = {10,2,30,14,5,155};
        assert Subtraction.getSubtraction(arr) == 94;
        assert Subtraction.getSubtraction(10,2,30,14,5,155) == 94;


    }
    @Test
    public void Test_3_Mult (){

        int[] arr = {15, 3, 4, 6, 7, 8, 9, 0};
        assert Multiplication.getMultiplication(arr) == 0;
        assert Multiplication.getMultiplication(2, 2, 2, 2, 2) == 32;


    }
    @Test
    public void Test_4_Div() {

        int[] arr = {160,4,10,2};
        assert Division.getDivision(arr) == 2;
        assert Division.getDivision(100, 50, 2, 5) == 2;


    }
    @Test
    public void Test_5_F() {


        assert Factorial.getFactorial(3) == 6;
        assert Factorial.getFactorial(10) == 3628800;


    }
}